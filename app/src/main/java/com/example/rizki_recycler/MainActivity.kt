package com.example.rizki_recycler

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.rizki_recycler.model.UserData
import com.example.rizki_recycler.view.UserAdapter
import com.google.android.material.floatingactionbutton.FloatingActionButton

class MainActivity : AppCompatActivity() {
    private lateinit var addBTN:FloatingActionButton
    private lateinit var rec:RecyclerView
    private lateinit var userList:ArrayList<UserData>
    private lateinit var userAdapter:UserAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /**Setting Variable*/
        addBTN = findViewById(R.id.addingBTN)
        rec = findViewById(R.id.mrecycler)
        userList = ArrayList()
        userAdapter = UserAdapter(this, userList)

        /**Setting Recycler View*/
        rec.layoutManager = LinearLayoutManager(this)
        rec.adapter = userAdapter

        /**Add dialog*/
        addBTN.setOnClickListener{addInfo()}
    }

    private fun addInfo(){
        val inflter = LayoutInflater.from(this)
        val v = inflter.inflate(R.layout.add_item, null)
        val userName = v.findViewById<EditText>(R.id.userName)
        val userNim = v.findViewById<EditText>(R.id.userNIM)
        val addDialog = AlertDialog.Builder(this)
        addDialog.setView(v)
        addDialog.setPositiveButton("Ok"){
            dialog,_->
            val name = userName.text.toString()
            val nim = userNim.text.toString()
            userList.add(UserData("Name: $name", "Nim: $nim"))
            userAdapter.notifyDataSetChanged()
            Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show()
            dialog.dismiss()
        }
        addDialog.setNegativeButton("Cancel"){
            dialog, _->
            dialog.dismiss()
            Toast.makeText(this, "Cancel", Toast.LENGTH_SHORT).show()

        }
        addDialog.create()
        addDialog.show()
    }
}